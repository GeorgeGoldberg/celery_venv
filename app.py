#Set up and run celery
from celery import Celery

celeryapp = Celery()
celeryapp.config_from_object('celery_config')
