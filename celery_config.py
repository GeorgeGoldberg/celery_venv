#import tasks and set up scheduler
imports = ('tasks')
task_ignore_result = False

result_backend = 'redis://localhost/0'
redis_host = '127.0.0.1'
redis_port = '6379'
broker_url = 'redis://localhost/0'

from datetime import timedelta

beat_schedule = {
    'say_hi_10s_intervals':{
        'task':'tasks.say_hi',
        'schedule':timedelta(seconds=10)
    }
}
