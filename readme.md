# Basic Celery with redis

## Overview

* This basic app is designed to show redis and celery operating together on a local machine. Celery executes the tasks queued on redis.

* celery_config.py imports a task (say_hi which prints "Hi") and schedules celery to run that function every 10 seconds via celery's "beat".

## Installation walkthrough (MacOS)  
``brew install redis``  
``brew install virtualenv``  
``virtualenv new_env`` (at home dir)  
``source new_env/bin/activate`` (use the VM)  
``pip install celery``  
``git clone ``(this repo)  
``cd`` to the cloned repo  


## Run app
* Open a second terminal window:
``redis-server``
* In the original window:
``celery worker -A app -l info --beat``

### Requirements

- Python
- celery python package
- redis server (host the queue)

## Notes
- ``-A app`` refers to app.py file here that imports the config and holds the interval (beat) scheduler
- Use ``redis-cli ping`` to check whether redis server is running
