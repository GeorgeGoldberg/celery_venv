#decorating function makes it available to config file
# as config file imports this tasks.py file


from celery import task

@task
def say_hi():
    print('Hi')
